from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request,"sitiocliente/home.html")

def catjuegos(request):
    return render(request,"sitiocliente/catalogojuegos.html")    