from django.urls import path
from .views import catjuegos, home



urlpatterns = [
    path('', home, name='home'),
    path('catjuegos/', catjuegos, name='catjuegos')
]

