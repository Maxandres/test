from django.apps import AppConfig


class SitioadminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sitioadmin'
